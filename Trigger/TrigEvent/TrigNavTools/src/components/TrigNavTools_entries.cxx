
#include "../StreamTrigNavSlimming.h"
#include "TrigNavTools/TrigNavigationSlimmingTool.h"
#include "../TrigNavigationSlimming.h"
#include "../TrigNavigationThinningTool.h"
#include "../TrigNavigationThinningSvc.h"


DECLARE_COMPONENT( HLT::TrigNavigationSlimmingTool )
DECLARE_COMPONENT( HLT::TrigNavigationSlimming )
DECLARE_COMPONENT( HLT::StreamTrigNavSlimming )
DECLARE_COMPONENT( TrigNavigationThinningTool )
DECLARE_COMPONENT( TrigNavigationThinningSvc )

